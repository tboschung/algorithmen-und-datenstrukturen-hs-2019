
//NB: KEIN Paket hinzufügen!
//NB: Es ist NICHT ERLAUBT, andere Klassen zu importieren. 
//NB: Es ist NICHT ERLAUBT, andere Klassen die nicht zu java.lang.* gehören zu verwenden. 

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

// BITTE VERÄNDERN SIE DIE ZEILEN OBERHALB NICHT! 

// NB: Bitte verändern Sie die untenstehenden Deklarationen der Klasse Main und der Methode main() NICHT, um das Programm auf dem Judge laufen zu lassen.
// Die Klasse muss als "class Main { ... }" deklariert sein. 
// Die Methode muss als "public static void main(String[] args) { ... }" deklariert sein.

class Main {
	// n ist die Anzahl Skulpturen in der Kunstgalerie
	// V ist die Kapazität des Lastwagens
	// T ist die verbleibende Zeit bis zum eintreffen der Polizei
	// Für i=1,...,n gilt: Die Elemente volume[i], time[i] und price[i] sind die
	// ganzen Zahlen (Integer) v_i, t_i und p_i der Aufgabenstellung.
	// volume[0], time[0] und price[0] werden nicht benutzt und sind gleich 0.
	static int solve(int n, int V, int T, int[] volume, int[] time, int[] price) {
		int[][][] DP = new int[n + 1][V+1][T+1];

		for (int i = 1; i <= n; i++) {
			for (int t = 0; t <= T; t++) {
				for (int v = 0; v <= V; v++) {

					if (volume[i] <= v && time[i] <= t) {
						DP[i][v][t] = max(DP[i - 1][v - volume[i]][t - time[i]] + price[i], DP[i - 1][v][t]);
					} else {
						DP[i][v][t] = DP[i - 1][v][t];
					}
				}
			}
		}
		return DP[n][V][T]; // TODO: SCHREIBEN SIE IHRE LÖSUNG HIER.
	}

	public static void main(String[] args) {
		read_and_solve(System.in, System.out);
	}

	public static int max(int a, int b) {
		return a > b ? a : b;
	}

	public static void read_and_solve(InputStream istream, PrintStream output) {
		Scanner scanner = new Scanner(istream);

		int ntestcases = scanner.nextInt();
		for (int t = 0; t < ntestcases; t++) {
			int n = scanner.nextInt();
			int V = scanner.nextInt();
			int T = scanner.nextInt();

			int[] volume = new int[n + 1];
			int[] time = new int[n + 1];
			int[] price = new int[n + 1];

			for (int i = 1; i <= n; i++) {
				volume[i] = scanner.nextInt();
				time[i] = scanner.nextInt();
				price[i] = scanner.nextInt();
			}

			output.println(solve(n, V, T, volume, time, price));
		}

		scanner.close();
	}
}