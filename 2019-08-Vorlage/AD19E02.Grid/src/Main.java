
// ==================================================================================================================
// Exercise   : AD19E02.Grid
// Submission : https://judge.inf.ethz.ch/team/websubmit.php?cid=28781&problem=AD19E02
// Author     : 
// ==================================================================================================================

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

class Main {
	//
	// Provide the solution of the grid problem in this function.
	// Feel free to provide additional fields and methods if
	// necessary.
	//
	public static int solveGrid(int[][] grid) {
		
		if(grid.length == 1) {
			return grid[0][0];
		}

		int[][] DP = new int[grid.length][grid.length];

		for (int x = 0; x < grid.length; x++) {
			DP[0][x] = grid[0][x];
		}
		
		int min = Integer.MAX_VALUE;

		for (int i = 1; i < grid.length; i++) {
			for (int j = 0; j < grid.length; j++) {
				if(j > 0 && j < grid.length - 1) {
					DP[i][j] = min(DP[i - 1][j], DP[i - 1][j + 1], DP[i - 1][j - 1]);
					DP[i][j] += grid[i][j];
				}
				else if(j == grid.length - 1) {
					DP[i][j] = min(DP[i - 1][j], DP[i - 1][j - 1]);
					DP[i][j] += grid[i][j];
				}
				else {
					DP[i][j] = min(DP[i - 1][j], DP[i - 1][j + 1]);
					DP[i][j] += grid[i][j];
				}
				if(i == grid.length-1) {
					min = min < DP[i][j] ? min : DP[i][j]; 
				}
			}
		}
		return min;
	}

	public static int min(int a, int b) {
		return a < b ? a : b;
	}

	public static int min(int a, int b, int c) {
		return min(a, b) < c ? min(a, b) : c;
	}

	public static int min(int[] array) {
		int min = array[0];

		for (int x = 1; x < array.length; x++) {
			min = min < array[x] ? min : array[x];
		}

		return min;
	}

	//
	// Please, do not modify the read_and_solve method, as well as the main method
	//
	public static void read_and_solve(InputStream in, PrintStream out) {
		//
		// Define a scanner that will read the input
		//
		Scanner scanner = new Scanner(in);
		//
		// Read the number of test cases, and start executing
		//
		int T = scanner.nextInt();
		for (int test = 0; test < T; test += 1) {
			//
			// Read the size of the array and create a new one
			//
			int N = scanner.nextInt();
			int[][] grid = new int[N][];
			for (int i = 0; i < N; i += 1) {
				grid[i] = new int[N];
				for (int j = 0; j < N; j += 1) {
					grid[i][j] = scanner.nextInt();
				}
			}
			out.println(solveGrid(grid));
		}
		scanner.close();
	}

	//
	// Do not modify the main method, and keep the method read_and_solve
	//
	public static void main(String[] args) {
		read_and_solve(System.in, System.out);
	}
}