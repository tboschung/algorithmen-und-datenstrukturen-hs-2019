//NB: Fügen Sie keine Pakete hinzu.

//NB: Das Importieren von anderen Klassen ist NICHT ERLAUBT.
//NB: Das Benützen anderer Klassen, die nicht zu java.lang.* gehören, ist NICHT ERLAUBT.
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

// BITTE VERÄNDERN SIE DEN OBENSTEHENDEN CODE NICHT.

//NB: Verändern Sie die untenstehende Deklaration der Klasse Main und der Methode main() NICHT, 
//sonst kann der Judge das Programm nicht laufen lassen. 
//Die Klasse MUSS als "class Main { ... }" deklariert werden, 
//die Methode als "public static void main(String[] args) { ... }".
class Main {

	// n bezeichnet die Anzahl der ALGO Steine.
	// l[i] enthält die Länge des i-ten ALGO Steins, für i=1,...,n
	// b[i] enthält die Breite des i-ten ALGO Steins, für i=1,...,n
	// h[i] enthält die Höhe des i-ten ALGO Steins, für i=1,...,n
	static int solve(int n, int[] l, int[] b, int[] h) {
		int[][] DP = new int[n][6];

		DP[0][0] = h[1];// base case
		DP[0][1] = l[1];
		DP[0][2] = b[1];

		for (int x = 1; x < n; x++) {

			if (wouldFit(x - 1, x, true, DP, l, b) && wouldFit(x - 1, x, false, DP, l, b)) {
				if (DP[x - 1][0] > DP[x - 1][3]) {

					DP[x][0] = DP[x - 1][0] + h[x + 1]; // highest fitting tower + extra height
					DP[x][1] = l[x + 1];
					DP[x][2] = b[x + 1];

				} else {

					DP[x][0] = DP[x - 1][3] + h[x + 1]; // highest fitting tower + extra height
					DP[x][1] = l[x + 1];
					DP[x][2] = b[x + 1];

				}
			} else if (wouldFit(x - 1, x, true, DP, l, b)) {

				DP[x][0] = DP[x - 1][0] + h[x + 1]; // highest fitting tower + extra height
				DP[x][1] = l[x + 1];
				DP[x][2] = b[x + 1];

			} else if (wouldFit(x - 1, x, false, DP, l, b)) {

				DP[x][0] = DP[x - 1][3] + h[x + 1]; // highest fitting tower + extra height
				DP[x][1] = l[x + 1];
				DP[x][2] = b[x + 1];

			} else { // find fitting tower
				
				for (int y = 1; y <= x; y++) {

					if (wouldFit(x - y, x, true, DP, l, b)) {// fits on older tower
						DP[x][0] = DP[x - y][0] + h[x + 1]; // highest fitting tower + extra height
						DP[x][1] = l[x + 1];
						DP[x][2] = b[x + 1];
					}
					else if (wouldFit(x - 1, x, false, DP, l, b)) {
						DP[x][0] = DP[x - y][3] + h[x + 1]; // highest fitting tower + extra height
						DP[x][1] = l[x + 1];
						DP[x][2] = b[x + 1];
					}
				}
				if (DP[x][0] == 0) { // brand new tower
					DP[x][0] = h[x + 1];
					DP[x][1] = l[x + 1];
					DP[x][2] = b[x + 1];
				}
			}
			DP[x][3] = DP[x - 1][0]; // dont take
			DP[x][4] = l[x];
			DP[x][5] = b[x];
		}
			return max(DP[n - 1][0], DP[n - 1][3]); // return the bigger value, (take n, dont take n)
	}

	public static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}

	public static boolean wouldFit(int base, int top, boolean bool, int[][] DP, int[] l, int[] b) { // bool is true for
																									// take tower
		top = top + 1;
		if (bool) {
			if (DP[base][1] >= l[top] && DP[base][2] >= b[top]) {
				return true;
			} else if (DP[base][2] >= l[top] && DP[base][1] >= b[top]) {
				return true;
			} else {
				return false;
			}
		} else {
			if (DP[base][4] >= l[top] && DP[base][5] >= b[top]) {
				return true;
			} else if (DP[base][5] >= l[top] && DP[base][4] >= b[top]) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static void main(String[] args) {
		read_and_solve(System.in, System.out);
	}

	public static void read_and_solve(InputStream istream, PrintStream output) {
		Scanner scanner = new Scanner(istream);

		int ntestcases = scanner.nextInt();
		for (int t = 0; t < ntestcases; t++) {
			int n = scanner.nextInt();

			int[] l = new int[n + 1];
			int[] b = new int[n + 1];
			int[] h = new int[n + 1];

			for (int i = 1; i <= n; i++)
				l[i] = scanner.nextInt();

			for (int i = 1; i <= n; i++)
				b[i] = scanner.nextInt();

			for (int i = 1; i <= n; i++)
				h[i] = scanner.nextInt();

			output.println(solve(n, l, b, h));
		}

		scanner.close();
	}
}
